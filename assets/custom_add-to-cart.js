// option to add multiple variants to cart
$('#addToCartBtn').click(function(){
    var jsonStr = '{"items":[]}';
    var obj = JSON.parse(jsonStr);
    var promises = [];
    $('.PDP-variant-lister div.variant').each(function(){
        var id = this.getAttribute('data-variant-id');
        let r = $(this).find('input').val();
        let val = this.getAttribute('data-product-id');
        let product_title = this.getAttribute('data-producttitle');
        product_title = product_title +' (maatwerk)';
        var product_price = $(this).find('.product_m2_adviesprijs').text().replace('€', '');
        var product_image = $('.list-product a img').attr('src');
        var vendor = this.getAttribute('data-vendor');
        var length = $(this).find('#length').val();
        var width = $(this).find('#width').val();
        if (parseInt(r) > 0) {
            $('.cart-override').addClass('cart_loading');
            if($(this)[0].className === 'PDP-custom variant') {
                promises.push($.ajax ({

                    url : "https://vloerkledenwinkel-cart.herokuapp.com/b2b-cart",
     
                    type: 'POST',
     
                    data: ({
                      'product_title' : product_title,
                      'vendor' : vendor,
                      'price' :product_price,
                      'product_img':product_image,
                      'weight':'0'
                    }),
     
                    success: function(data){
                        var parsedData = JSON.parse(data);
                        if (parsedData.response == 'success') {
                            obj['items'].push({"id": parsedData.id, "quantity": JSON.parse(r), "properties": 
                            {
                                'Maatwerk': 'yes',
                                'Lengte':length,
                                'Breedte':width
                            }});
                        }
                    }
                }));
            } else {
                obj['items'].push({"id": JSON.parse(id), "quantity": JSON.parse(r)})
            }
        }
    });

    Promise.all(promises).then(()=>{
        fetch('/cart/add.js', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(obj)
        })
        .then(response => {
            $('.cart-override').removeClass('cart_loading');
            $("#header-cart")[0].click();
            return response.json();
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    });
    
});

$('#staal-btn').click(function(){
    var jsonStr = '{"items":[]}';
    var obj = JSON.parse(jsonStr);
    var id = this.getAttribute('data-staal-id');

    obj['items'].push({"id": JSON.parse(id)})

    console.log(obj)

    fetch('/cart/add.js', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(obj)
    })
        .then(response => {
            return response.json();
        })
        .catch((error) => {
            console.error('Error:', error);
        });
});

$('#addToCartStalen').click(function(){
    var jsonStr = '{"items":[]}';
    var obj = JSON.parse(jsonStr);

    $('.PDP-variant-lister div.variant').each(function(){
        var id = this.getAttribute('data-variant-id');
        let r = $(this).find('input').val();
        let val = this.getAttribute('data-product-id');
        let product_title = this.getAttribute('data-producttitle');
        if (parseInt(r) > 0) {
            if($(this)[0].className === 'PDP-custom variant') {
                obj['items'].push({"id": JSON.parse(val), "quantity": JSON.parse(r), "properties": {'product_title':product_title}})
            } else {
                obj['items'].push({"id": JSON.parse(id), "quantity": JSON.parse(r)})
            }
        }
    });

    console.log("product objStaal: ", obj);

    fetch('/cart/add.js', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(obj)
    })
        .then(response => {
            return response.json();
        })
        .catch((error) => {
            console.error('Error:', error);
        });
});

// clear cart on logout
$(function() {
    $('#clear-cart').on('click',function(e){
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: '/cart/clear.js',
            success: function(){
                window.location.href =  '/account/logout';
            },
            dataType: 'json'
        });
    })
});
