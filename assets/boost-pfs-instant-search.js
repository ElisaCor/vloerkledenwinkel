// Override Settings
var boostPFSInstantSearchConfig = {
	search: {
		//suggestionMode: 'test',
		//suggestionPosition: 'left'
	},
    label: {
      search: {
        seoTitleOne: 'Zoekresultaten voor &quot;{{ terms }}&quot; - Vloerkledenwinkel',
        seoTitleOther: 'Zoekresultaten voor &quot;{{ terms }}&quot; - Vloerkledenwinkel',
      }
    }
};

(function() {
	BoostPFS.inject(this);

	// Customize style of Suggestion box
	SearchInput.prototype.customizeInstantSearch = function() {
		var suggestionElement = this.$uiMenuElement;
		var searchElement = this.$element;
		var searchBoxId = this.id;

    var viewAllResults = jQuery('.boost-pfs-search-suggestion-wrapper .boost-pfs-search-suggestion-header-view-all.boost-pfs-search-suggestion-header');

        if (viewAllResults.length) {
          var kn = sessionStorage.getItem('killNotification');
          if (kn) {
            if (document.getElementsByClassName("boost-pfs-search-suggestion-wrapper")[0]) {
              document.getElementsByClassName("boost-pfs-search-suggestion-wrapper")[0].classList.add('move-search-container');
            }
            if (document.getElementsByClassName("boost-pfs-search-suggestion-wrapper")[1]) {
              document.getElementsByClassName("boost-pfs-search-suggestion-wrapper")[1].classList.add('move-search-container');
            }
            if (document.getElementsByClassName("boost-pfs-search-suggestion-wrapper")[2]) {
              document.getElementsByClassName("boost-pfs-search-suggestion-wrapper")[2].classList.add('move-search-container');
            }
            if (document.getElementsByClassName("boost-pfs-search-suggestion-wrapper")[3]) {
              document.getElementsByClassName("boost-pfs-search-suggestion-wrapper")[3].classList.add('move-search-container');
            }
            if (document.getElementsByClassName("boost-pfs-search-suggestion-wrapper")[4]) {
              document.getElementsByClassName("boost-pfs-search-suggestion-wrapper")[5].classList.add('move-search-container');
            }
            if (document.getElementsByClassName("boost-pfs-search-suggestion-wrapper")[6]) {
              document.getElementsByClassName("boost-pfs-search-suggestion-wrapper")[6].classList.add('move-search-container');
            }
          }

          viewAllResults.find('count').remove();
          var data = this.instantSearchResult.data;
          for (var i = 0; i < data.length; i++) {
            if (data[i].key == 'total_product') {
               viewAllResults.children('a').append('<span class="count">(' + data[i].values + ')</span>');
            }
          }
        }

      
//       var length = jQuery('.boost-pfs-search-suggestion-wrapper .boost-pfs-search-suggestion-group ul .boost-pfs-search-suggestion-item-product').length;
//       var lastItem = jQuery('.boost-pfs-search-suggestion-wrapper .boost-pfs-search-suggestion-group ul .boost-pfs-search-suggestion-item-product').last();
//       var firstTwo = jQuery(jQuery('.boost-pfs-search-suggestion-wrapper .boost-pfs-search-suggestion-group ul .boost-pfs-search-suggestion-item-product').slice(0,2).get().reverse());
//       firstTwo.each(function() {
//         jQuery(this).clone().addClass('with-image').insertAfter(lastItem);
//         if (length < 3) {
//         	jQuery(this).remove();
//         }
//       });
      	
	};

})();