function StartLiveChat(){
    window.Trengo.Api.Widget.open('chat');
}

function IsMobile(){
    var isMobile = (/iphone|ipod|android|ie|blackberry|fennec/).test
         (navigator.userAgent.toLowerCase());
    return isMobile;
}

function OpenTab(event, tabName) {
    sessionStorage.setItem('tabClicked', tabName);
    var i, tabContent, tabItem;

    tabContent = document.getElementsByClassName("tab-account-content");
    for (i = 0; i < tabContent.length; i++) {
        tabContent[i].style.display = "none";
    }

    tabItem = document.getElementsByClassName("tab-account-item");
    for (i = 0; i < tabItem.length; i++) {
        tabItem[i].className = tabItem[i].className.replace(" active", "");
    }

    document.getElementById(tabName).style.display = "block";
    if (event) {
        event.currentTarget.className += " active";
    }
    SmoothScrollToTarget();
}

function GoToOrders(){
    document.getElementById("tab-account-orders").click();
    SmoothScrollToTarget("true");
}

function GoToContact(){
    document.getElementById("tab-account-contact").click();
    SmoothScrollToTarget("true");
}

function SmoothScrollToTarget(forcedScroll){
    if (!IsMobile()){
        return;
    }
    var isTouch = (('ontouchstart' in window) || (navigator.msMaxTouchPoints > 0));
    if (isTouch || forcedScroll == "true") {
        var offset = -70;
        var target = document.querySelector('#scrollTo');
        window.scroll({ top: (target.offsetTop - offset), left: 0, behavior: 'smooth' });
    }
}
