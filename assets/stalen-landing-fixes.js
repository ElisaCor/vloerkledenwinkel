function ExecuteStaalStyling(){
    console.log("ExecuteStaalStyling");
    $(".stalen-float-right").css("margin-right", "80px");
    $(".PDP-variant-lister>.variant").css("justify-content", "flex-start");
    $(".PDP-variant-lister .prices-wrapper").css("width", "100%");
    $(".input-variant").css("width", "auto");
    $(".input-price-inkoop").css("display", "none");
    $(".input-adviesprijs").css("margin-left", "auto");
    $(".input-adviesprijs").css("color", "var(--razz)");
    $(".input-price").css("margin-left", "auto");
    $( window ).resize(function() {
    if ($(window).width() > 1188) {
        $(".input-variant").css("margin-left", "6px");
        $(".input-price-inkoop").css("margin-left", "auto");
    }
    else {
        $(".input-variant").css("margin-left", "43px");
        $(".input-price-inkoop").css("margin-left", "43px");
    }
    });
    if ($(window).width() > 1188) {
        $(".input-variant").css("margin-left", "6px");
        $(".input-price-inkoop").css("margin-left", "auto");
    }
    else {
        $(".input-variant").css("margin-left", "43px");
        $(".input-price-inkoop").css("margin-left", "43px");
    }
}


function ExecuteLockedStyling() {
    console.log("ExecuteLockedStyling");
    $(".stalen-float-right").css("margin-right", "75px");
    $(".input-price.input-price-locked").css("width", "auto");
    $(".input-adviesprijs").css("color", "var(--razz)");
    // $(".input-adviesprijs").css("display", "flex");
    
    $( window ).resize(function() {
        if ($(window).width() > 1188) {
            $(".input-price.input-price-locked").css("margin-right", "110px");
        }
        else {
            $(".input-price.input-price-locked").css("margin-left", "38px");
        }
    });
    if ($(window).width() > 1188) {
        $(".input-price.input-price-locked").css("margin-right", "110px");
    }
    else {
        $(".input-price.input-price-locked").css("margin-left", "38px");
    }
}

function ExecuteNoInventoryStyling() {
    console.log("ExecuteNoInventoryStyling");
    
    $(".stalen-float-right").css("margin-right", "185px");
    $(".PDP-variant-lister>.variant").css("justify-content", "flex-start");
    $(".prices-wrapper").css("width", "100%");

    $(".variant-lister-input-container.variant-lister-input-container-absent").css("display", "none");
    
    $(".input-variant.price-no-inventory").css("width", "auto");
    $(".input-price-inkoop.price-no-inventory").css("width", "48px");
    $(".input-adviesprijs.price-no-inventory").css("display", "none");
    $("span#adviesprijs-mobile").css("display", "none");
    $(".input-adviesprijs.price-no-inventory").css("margin-left", "auto");
    $(".input-price.price-no-inventory").css("margin-left", "auto");

    $( window ).resize(function() {
    if ($(window).width() > 1188) {
        $(".input-variant.price-no-inventory").css("margin-left", "6px");
        $(".input-price-inkoop.price-no-inventory").css("margin-left", "auto");
    }
    else {
        $(".input-variant.price-no-inventory").css("margin-left", "43px");
        $(".input-price-inkoop.price-no-inventory").css("margin-left", "43px");
    }
    });
    if ($(window).width() > 1188) {
        $(".input-variant.price-no-inventory").css("margin-left", "6px");
        $(".input-price-inkoop.price-no-inventory").css("margin-left", "auto");
    }
    else {
        $(".input-variant.price-no-inventory").css("margin-left", "43px");
        $(".input-price-inkoop.price-no-inventory").css("margin-left", "43px");
    }

    $( window ).resize(function() {
        if ($(window).width() > 1188) {
            $(".input-price.input-price-locked.price-no-inventory").css("margin-right", "110px");
        }
        else {
            $(".input-price.input-price-locked.price-no-inventory").css("margin-left", "38px");
            $(".input-inventory.not-available").css("margin-right", "auto");
            $(".input-inventory.not-available").css("margin-left", "43px !important");
        }
    });
    if ($(window).width() > 1188) {
        $(".input-price.input-price-locked .price-no-inventory").css("margin-right", "110px");
    }
    else {
        $(".input-price.input-price-locked .price-no-inventory").css("margin-left", "38px");
        $(".input-inventory.not-available").css("margin-right", "auto");
        $(".input-inventory.not-available").css("margin-left", "43px !important");
    }
}
function ExecuteLockedStylingWithAvailable() {
    console.log("ExecuteLockedStylingWithAvailable");
    $(".stalen-float-right").css("margin-right", "205px");
    $(".input-price.input-price-locked").css("width", "auto");
    // $(".input-adviesprijs").css("color", "var(--razz)");
    // $(".input-adviesprijs").css("display", "flex");
    
    $( window ).resize(function() {
        if ($(window).width() > 1188) {
            $(".input-price.input-price-locked").css("margin-right", "110px");
        }
        else {
            $(".input-price.input-price-locked").css("margin-left", "38px");
        }
    });
    if ($(window).width() > 1188) {
        $(".input-price.input-price-locked").css("margin-right", "110px");
    }
    else {
        $(".input-price.input-price-locked").css("margin-left", "38px");
    }
}

function NoCustomerStyling() {
    $(".stalen-float-right").css("margin-right", "193px");
}
