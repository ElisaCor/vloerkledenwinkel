const targetDiv = document.getElementsByClassName("soldoutInfo");
const btn = document.getElementsByClassName("toggleInfo");

for (let i = 0; i < btn.length; i++) {
    btn[i].onclick = function () {
        btn_clicked =  $(this)[0].getAttribute('data-counter')

        for (let i = 0; i < targetDiv.length; i++) {
            if(targetDiv[i].getAttribute('data-counter') === btn_clicked) {
                if (targetDiv[i].style.display !== "none" && targetDiv[i].style.display !== '') {
                    targetDiv[i].style.display = "none";
                } else {
                    targetDiv[i].style.display = "flex";
                }
            }
        }

    };
}
