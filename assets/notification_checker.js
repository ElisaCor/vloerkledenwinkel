$(document).ready(function() {
    CheckForKillNotification();
});

function CheckForKillNotification() {
    var kn = sessionStorage.getItem('killNotification');
    if (kn) {
        killTopNotification();
    }
    else{
        allowTopNotification();
    }
}

function setNotificationSettings() {
    sessionStorage.setItem('killNotification', 'true');
    $("#shopify-section-message-bar").fadeOut("fast", function() {
        killTopNotification();
    });
}

function allowTopNotification() {
    $(document).ready(function() {
        document.getElementsByClassName("message-bar-container")[0].classList.remove('kill-top-notification');
        document.getElementById("shopify-section-message-bar").classList.remove('kill-top-notification');
        document.getElementById("top").classList.remove('move-top');
        document.getElementById("content").classList.remove('move-content');
    });
}

function killTopNotification() {
    $(document).ready(function() {
        document.getElementById("shopify-section-message-bar").classList.add('kill-top-notification');
        document.getElementById("top").classList.add('move-top');
        document.getElementById("content").classList.add('move-content');
        
        var topContainer = document.getElementsByClassName("top-container");
        for(var i = 0; i < topContainer.length; i++)
        {
            topContainer[i].className += "move-mobile-menu";
        }
        document.getElementsByClassName("boost-pfs-search-suggestion-wrapper")[0].classList.add('move-search-container');
    });
}

